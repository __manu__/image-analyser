import os

from loguru import logger

from app.config import SystemConfig
from app.service.s3_service import S3Service


class ModelServer(S3Service):

    def __init__(self):
        super().__init__(SystemConfig.get_vital("aws_access_key_id"),
                         SystemConfig.get_vital("aws_secret_access_key"),
                         SystemConfig.get_vital("aws_region"))
        self.model_config = {}
        self.model_labels = SystemConfig.get_vital("model_markers").split(",")
        self.bucket = SystemConfig.get_vital("model_bucket_s3")
        self.refresh_always = SystemConfig.get("model_refresh_if_not_present", "true") == "false"
        self.local = SystemConfig.get_vital("model_bucket_local")

    def refresh(self):
        self.refresh_forced(self.refresh_always)

    def refresh_forced(self, refresh_always: bool):
        os.makedirs(self.local, exist_ok=True)
        self.model_config = self._load_model_config(self.model_labels)
        self._load_models(self.model_labels, self.model_config, refresh_always)

    def _load_model_config(self, model_labels) -> {}:
        model_config = {}
        for model_label in model_labels:
            configs = SystemConfig.get_vital(f"{model_label}_config").split(",")
            model_config[model_label] = {}
            for config in configs:
                key = f"{model_label}_{config}"
                model_config[model_label][config] = SystemConfig.get_vital(key)
        return model_config

    def _load_models(self, model_labels, model_config, refresh_always: bool):
        for model_label in model_labels:
            model_path = model_config[model_label]['model']
            model_path_local = f"{self.local}/{model_label}/{model_path}"
            model_config[model_label]['model_local'] = model_path_local
            if not os.path.exists(model_path_local) or refresh_always:
                logger.info("Model-{} download starts", model_label)
                self.download(model_path_local, self.bucket, model_path)
                logger.info("Model-{} download complete", model_label)
            else:
                logger.warning("Model-{} not downloaded due to its presence", model_label)

    def get_config(self, model_label: str) -> {}:
        return self.model_config[model_label]

    def get_local_model(self, model_label: str) -> str:
        return self.model_config[model_label]['model_local']

    def get_rule_config(self, model_label: str) -> str:
        return self.model_config[model_label]['rule_config']
