import json

from json_logic import jsonLogic
from loguru import logger

from app.config import SystemConfig
from app.interface.rule_processor import RuleProcessor
from app.model.processor import ProcessingStatus, MRCNNImageProcessingStatus, StatusResponse, ProcessorStatus
from app.model.rule import RulesData
from app.service.s3_service import S3Service
from app.utils.type_utils import add_lists


class MRCNNRuleProcessor(RuleProcessor):
    rules_data: RulesData
    store: S3Service
    rule_config: str

    def __init__(self, rule_config: str) -> None:
        super().__init__()
        self.store = S3Service(SystemConfig.get_vital("aws_access_key_id"),
                               SystemConfig.get_vital("aws_secret_access_key"),
                               SystemConfig.get_vital("aws_region"))
        self.bucket = SystemConfig.get_vital("default_s3_bucket")
        self.store.download(
            rule_config,
            self.bucket,
            rule_config
        )
        self.rule_config = rule_config
        self._init()

    def _init(self):
        logger.info("Initialising rules")
        self.rules_data = RulesData.parse_file(self.rule_config)

    def run(self, image_processing_status: MRCNNImageProcessingStatus) -> ProcessingStatus:
        self._apply_rules(image_processing_status)
        MRCNNRuleProcessor.mark_status(image_processing_status)
        return image_processing_status

    def _apply_rules(self, image_processing_status):
        for rule in self.rules_data.rules:
            if jsonLogic(rule.when, image_processing_status.detections.dict()):
                then = rule.then
                image_processing_status.errors = add_lists(image_processing_status.errors, then.errors)
                image_processing_status.warn = add_lists(image_processing_status.warn, then.warn)
                image_processing_status.info = add_lists(image_processing_status.info, then.info)
                image_processing_status.instructions = add_lists(image_processing_status.instructions,
                                                                 then.instructions)

    def update(self, rules: RulesData):
        try:
            with open(self.rule_config, 'w') as f:
                json.dump(rules.dict(), f)
            self.store.upload(self.rule_config, self.bucket, self.rule_config)
            self._init()
            return StatusResponse(status=ProcessorStatus.SUCCESS)
        except Exception as e:
            logger.error("Failed to update MRCNN rules: {}", e)
            return StatusResponse(status=ProcessorStatus.FAILURE)

    @staticmethod
    def mark_status(image_processing_status):
        if image_processing_status.errors:
            image_processing_status.status = ProcessorStatus.FAILURE

    def get(self) -> RulesData:
        return self.rules_data
