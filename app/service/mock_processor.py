import time
from typing import Optional

from starlette.background import BackgroundTasks

from app.interface.processor import ImageProcessor
from app.interface.rule_processor import RuleProcessor
from app.model.processor import ProcessingStatus, ProcessorStatus, ImageProcessingStatus, StatusResponse
from app.model.rule import RulesData


class MockProcessor(ImageProcessor, RuleProcessor):

    def get(self) -> RulesData:
        return None

    def update(self, rules: RulesData, label: Optional[str]) -> StatusResponse:
        return StatusResponse(status=ProcessorStatus.FAILURE)

    def run(self, image_processing_status: ImageProcessingStatus) -> ProcessingStatus:
        return image_processing_status

    def process(self, image_id: str, image_uri: str, background_tasks: BackgroundTasks) -> ProcessingStatus:
        time.sleep(5)
        if "err" in image_id:
            return ProcessingStatus(image_id=image_id, status=ProcessorStatus.FAILURE,
                                    errors=["Error 1", "Error 2"],
                                    instructions=["Do this", "Do something crazy"])
        elif "warn" in image_id:
            return ProcessingStatus(image_id=image_id, status=ProcessorStatus.SUCCESS, warn=["Warning 1", "Warning 2"])
        elif "def" in image_id:
            return ProcessingStatus(image_id=image_id, status=ProcessorStatus.DEFERRED)
        else:
            return ProcessingStatus(image_id=image_id, status=ProcessorStatus.SUCCESS)
