import os

import boto3
from botocore.client import BaseClient
from botocore.exceptions import ClientError
from loguru import logger


class S3Service(object):

    def __init__(self, aws_access_key_id, aws_secret_access_key, region, *args, **kwargs):
        self.aws_access_key_id = aws_access_key_id
        self.aws_secret_access_key = aws_secret_access_key
        self.region = region

    def upload_obj(self, fileobject, bucket, key):
        try:
            client: BaseClient = self.get_client()
            client.upload_fileobj(fileobject, bucket, key)
            return True
        except ClientError as e:
            logger.error("Failed to upload file-{}: {}", key, e)
        return False

    def upload(self, file_name, bucket, key):
        try:
            client: BaseClient = self.get_client()
            client.upload_file(file_name, bucket, key)
            return True
        except ClientError as e:
            logger.error("Failed to upload file-{}: {}", key, e)
        return False

    def get_client(self) -> BaseClient:
        return boto3.client(service_name='s3', region_name=self.region,
                            aws_secret_access_key=self.aws_secret_access_key,
                            aws_access_key_id=self.aws_access_key_id)

    def download(self, save_as, bucket, key):
        try:
            client: BaseClient = self.get_client()
            dirname = os.path.dirname(save_as)
            os.makedirs(dirname, exist_ok=True)
            client.download_file(bucket, key, save_as)
            return True
        except ClientError as e:
            logger.error("Failed to download file-{}: {}", key, e)
        return False
