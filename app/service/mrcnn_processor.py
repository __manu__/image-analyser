import os
import time
import uuid

import cv2
import imutils
import keras.backend as K
import numpy as np
import skimage.io as io
import tensorflow as tf
from loguru import logger
from starlette.background import BackgroundTasks

import app.mrcnn.model as modellib
from app.config import SystemConfig
from app.exception.detection_exception import NoDetectionException
from app.interface.processor import ImageProcessor
from app.model.processor import ProcessorStatus, MRCNNImageProcessingStatus
from app.mrcnn import visualize
from app.mrcnn.config import Config
from app.service.image_store import ImageStore
from app.utils.path_utils import PathUtils

K.manual_variable_initialization(True)
os.environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true'
# Use gpu device
os.environ["CUDA_VISIBLE_DEVICES"] = SystemConfig.get_vital("gpu_visible")
graph = tf.compat.v1.get_default_graph()


class InferenceConfig(Config):
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1
    DETECTION_MIN_CONFIDENCE = 0.85
    NAME = "retractor"
    NUM_CLASSES = 1 + 5  # 4 labels
    # STEPS_PER_EPOCH = 178
    MAX_GT_INSTANCES = 10
    IMAGE_RESIZE_MODE = "square"
    IMAGE_MIN_DIM = 750
    IMAGE_MAX_DIM = 960


class MRCNNProcessor(ImageProcessor):
    def __init__(self, model_config: {}, model_local_path: str) -> None:
        super().__init__()

        self.device = "/CPU:0"
        if tf.test.is_gpu_available():
            self.device = "/GPU:0"

        if tf.test.gpu_device_name():
            logger.info('Default GPU Device: {}'.format(tf.test.gpu_device_name()))
        else:
            logger.info("Tensorflow not compatible for GPU")
        self.blur_threshold = int(model_config["blur_threshold"])
        self.class_names = model_config["class_names"].split(",")
        self.fieldnames = model_config["field_names"].split(",")
        self.config = InferenceConfig()
        self.config.GPU_COUNT = int(SystemConfig.get_vital("gpu_count"))
        self.config.IMAGES_PER_GPU = int(model_config["images_per_gpu"])
        self.config.DETECTION_MIN_CONFIDENCE = float(
            model_config["detection_min_confidence"]
        )
        self.model_path = model_local_path
        self.image_out_store = SystemConfig.get_vital("default_image_out")
        self.image_store = ImageStore()
        self.is_crop = SystemConfig.get_vital("crop_result_image") == "true"
        self.image_store_overwrite = SystemConfig.get_vital("image_store_overwrite") == "true"
        logger.debug("Configuration : {}", self.config.display())
        with tf.device(self.device):
            try:
                with graph.as_default():
                    self.model = modellib.MaskRCNN(mode="inference", config=self.config)
                    self.model.load_weights(self.model_path, by_name=True)
                    self.model.keras_model._make_predict_function()
                    logger.info("Loaded ML Model with config {}".format(self.model.config))
            except Exception as e:
                logger.error("Failed to load weights: {}", e)

    def process(self, image_id: str, image_uri: str, background_tasks: BackgroundTasks) -> MRCNNImageProcessingStatus:
        status = self.do_process(image_id, image_uri)
        if status.result_image_uri:
            upload_id = image_id if self.image_store_overwrite else f"{image_id}_{uuid.uuid4().hex[:6].upper()}.{PathUtils.get_extension(image_id)}"
            background_tasks.add_task(self.image_store.upload_image, status.result_image_uri, upload_id)
            status.result_id = upload_id
        return status

    def do_process(self, image_id: str, image_url: str) -> MRCNNImageProcessingStatus:

        try:
            status = self.get_detections(image_id, image_url)
            logger.info("Detections: {}", status.detections, status)
            return status
        except NoDetectionException as x:
            logger.error("Failed to get detections: {}", x)
            return self.detection_error(image_id)
        except Exception as e:
            logger.error("Failed to analyse detections due to system error {}:{}", e, str(e))
            return MRCNNImageProcessingStatus(
                image_id=image_id,
                status=ProcessorStatus.FAILURE,
                errors=["Please contact administrator"],
            )

    def get_detections(self, image_id, image_path) -> MRCNNImageProcessingStatus:
        image = cv2.imread(image_path, cv2.IMREAD_UNCHANGED)
        (mean, blurry) = MRCNNProcessor.detect_blur(image, size=60, thresh=self.blur_threshold)
        logger.debug("Mean: {} | Blurry: {}", mean, blurry)
        error = []
        if blurry:
            logger.error("Unqualified image {}", image_path)
            return MRCNNProcessor.give_blurry_response(image_id)
        else:
            # Run detection
            logger.debug("Starting the inference.")
            start = time.time()
            image = io.imread(image_path)
            r = self.model.detect([image], verbose=0)[0]
            logger.debug("Detection {}".format(r))
            stored_path = f"{self.image_out_store}/{image_path}"
            PathUtils.create_parent_path(stored_path)
            mrcnn_detection = visualize.save_detections(image_path,
                                                        self.image_out_store,
                                                        image,
                                                        r['rois'], r['masks'], r['class_ids'],
                                                        self.class_names, r['scores'],
                                                        crop=self.is_crop)
            end = time.time()
            logger.info("Inference time {} sec".format(round(end - start, 2)))
            logger.debug("Detections {}".format(mrcnn_detection))
        logger.debug("API Response: {}".format(MRCNNImageProcessingStatus(image_id=image_id,
                                                                          image_uri=image_path,
                                                                          status=ProcessorStatus.SUCCESS,
                                                                          blurry=False,
                                                                          detections=mrcnn_detection,
                                                                          result_image_uri=stored_path)))
        return MRCNNImageProcessingStatus(errors=error,
                                          image_id=image_id,
                                          image_uri=image_path,
                                          status=ProcessorStatus.SUCCESS,
                                          blurry=False,
                                          detections=mrcnn_detection,
                                          result_image_uri=stored_path)

    @staticmethod
    def give_blurry_response(image_id):
        return MRCNNImageProcessingStatus(image_id=image_id, status=ProcessorStatus.FAILURE, blurry=True,
                                          errors=["Image is blurry"])

    @staticmethod
    def detection_error(image_id):
        return MRCNNImageProcessingStatus(image_id=image_id, status=ProcessorStatus.FAILURE, blurry=False,
                                          errors=["Neither teeth nor retractors identified"])

    @staticmethod
    def detect_blur(img, size=60, thresh=60):
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        resized_img = imutils.resize(gray, width=500)
        (cX, cY) = (int(resized_img.shape[1] / 2.0), int(resized_img.shape[0] / 2.0))
        fft = np.fft.fft2(resized_img)
        fft_shift = np.fft.fftshift(fft)
        fft_shift[cY - size:cY + size, cX - size:cX + size] = 0
        fft_shift = np.fft.ifftshift(fft_shift)
        recon = np.fft.ifft2(fft_shift)
        magnitude = 20 * np.log(np.abs(recon))
        mean = np.mean(magnitude)
        return mean, mean <= thresh
