import uuid

from app.interface.engine import Engine
from app.service.image_store import ImageStore


class DefaultEngine(Engine):

    def __init__(self) -> None:
        super().__init__()
        self.image_store = ImageStore()

    def get_image(self, image_id) -> str:
        image_uri = f"tmp/{uuid.uuid4()}/{image_id}"
        self.image_store.download_image(image_uri, image_id)
        return image_uri
