from app.config import SystemConfig
from app.service.s3_service import S3Service


class ImageStore(S3Service):

    def __init__(self):
        super().__init__(SystemConfig.get_vital("aws_access_key_id"),
                         SystemConfig.get_vital("aws_secret_access_key"),
                         SystemConfig.get_vital("aws_region"))
        self.bucket = SystemConfig.get_vital("default_s3_bucket")

    def download_image(self, save_as, key):
        self.download(save_as, self.bucket, key)

    def upload_image_obj(self, fileobject, key):
        self.upload_obj(fileobject, self.bucket, key)

    def upload_image(self, file_name, key):
        self.upload(file_name, self.bucket, key)