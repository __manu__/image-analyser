def add_lists(base_list: [], appendable: []):
    if appendable:
        base_list = base_list or []
        base_list.extend(appendable)
    return base_list
