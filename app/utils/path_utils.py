import ntpath
import os


class PathUtils(object):

    @staticmethod
    def create_paths(path: str):
        os.makedirs(path, exist_ok=True)

    @staticmethod
    def create_parent_path(file: str):
        os.makedirs(PathUtils.get_dir(file), exist_ok=True)

    @staticmethod
    def get_dir(file: str):
        return os.path.dirname(file)

    @staticmethod
    def get_name(file: str):
        return ntpath.basename(file)

    @staticmethod
    def get_extension(file: str):
        return os.path.splitext(file)[1][1:]
