from PIL import Image
from PIL.ImageFile import ImageFile
from loguru import logger

EXIF_ORIENTATION_TAG = 274
ImageFile.MAXBLOCK = 1024 * 1024
ORIENTATIONS = {
    1: ("Normal", 0),
    2: ("Mirrored left-to-right", 0),
    3: ("Rotated 180 degrees", Image.ROTATE_180),
    4: ("Mirrored top-to-bottom", 0),
    5: ("Mirrored along top-left diagonal", 0),
    6: ("Rotated 90 degrees", Image.ROTATE_270),
    7: ("Mirrored along top-right diagonal", 0),
    8: ("Rotated 270 degrees", Image.ROTATE_90)
}


def auto_rotate(img, over_write=False) -> bool:
    path = None
    if not isinstance(img, Image.Image):
        path = img
        img = Image.open(path).convert('RGB')
    elif over_write:
        raise ValueError("You can't use `overwrite` when passing an Image instance.  Use a file path instead.")
    try:
        orientation = img._getexif()[EXIF_ORIENTATION_TAG]
        if orientation in [3, 6, 8]:
            degrees = ORIENTATIONS[orientation][1]
            org_exif = img.info['exif']
            img = img.transpose(degrees)
            if over_write and path is not None:
                img.save(path, quality=100, exif=org_exif)
                return True
    except (TypeError, AttributeError, KeyError):
        logger.error("No EXIF data for {}", img)
    return False
