import abc

from starlette.background import BackgroundTasks

from app.model.processor import ProcessingStatus, ProcessorStatus, ImageProcessingStatus
from app.resolver.processor import ProcessorResolver, Processor


class Engine(abc.ABC):

    def __init__(self):
        super().__init__()

    @abc.abstractmethod
    def get_image(self, image_id: str) -> str:
        pass

    def process(self, image_id: str, background_tasks: BackgroundTasks) -> ProcessingStatus:
        return self._run(ProcessorResolver.get(), image_id, self.get_image(image_id=image_id), background_tasks)

    def _run(self,
             processor: Processor,
             image_id: str,
             image_uri: str,
             background_tasks: BackgroundTasks) -> ProcessingStatus:
        image_processing_status: ImageProcessingStatus = processor.image_processor.process(image_id,
                                                                                           image_uri,
                                                                                           background_tasks)
        return processor.rule_processor.run(image_processing_status) \
            if image_processing_status.can_proceed() \
            else image_processing_status

    def process_by_label(self, model_label: str, image_id: str, background_tasks: BackgroundTasks) -> ProcessingStatus:
        label = ProcessorResolver.get_by_label(model_label)
        return self._run(label, image_id, self.get_image(image_id=image_id), background_tasks) \
            if label \
            else ProcessingStatus(image_id=image_id,
                                  model_label=model_label,
                                  status=ProcessorStatus.FAILURE,
                                  errors=["Model not found"])
