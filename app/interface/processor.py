import abc

from starlette.background import BackgroundTasks

from app.model.processor import ImageProcessingStatus


class ImageProcessor(abc.ABC):

    @abc.abstractmethod
    def process(self, image_id: str, image_uri: str, background_tasks: BackgroundTasks) -> ImageProcessingStatus:
        pass
