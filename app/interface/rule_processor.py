import abc

from app.model.processor import ProcessingStatus, ImageProcessingStatus, StatusResponse
from app.model.rule import RulesData


class RuleProcessor(abc.ABC):

    @abc.abstractmethod
    def run(self, image_processing_status: ImageProcessingStatus) -> ProcessingStatus:
        pass

    @abc.abstractmethod
    def update(self, rules: RulesData) -> StatusResponse:
        pass

    @abc.abstractmethod
    def get(self) -> RulesData:
        pass