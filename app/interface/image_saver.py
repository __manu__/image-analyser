import abc

from app.model.processor import ProcessingStatus


class ImageSave(abc.ABC):

    @abc.abstractmethod
    def save(self, image_id: str, image_url: str) -> ProcessingStatus:
        pass
