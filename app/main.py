import os
import warnings

import tensorflow as tf
from fastapi import FastAPI
from fastapi.responses import RedirectResponse
from loguru import logger

from app.accessories.lifecycle_hooks import LifeCycle
from app.controller.v1.model_server import router as model_api
from app.controller.v1.processor import router as processor_api
from app.controller.v1.rule import router as rule_api
from app.controller.v1.test import router as test_api

warnings.filterwarnings("ignore")
tf.get_logger().disabled = True
tf.autograph.set_verbosity(3)
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
os.environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true'
logger.add("./logs/image-resolver.log", rotation="5 MB")
logger.info("Initializing application : image-resolver")

app = FastAPI(
    title="Image-Analyser",
    description="Its in my name",
    version="0.1.0"
)

logger.info("Adding processor api route")
app.include_router(processor_api)
logger.info("Adding test api route")
app.include_router(test_api)
logger.info("Adding model api route")
app.include_router(model_api)
logger.info("Adding rule api route")
app.include_router(rule_api)


@app.on_event("startup")
def startup():
    logger.info("System startup initiated")
    LifeCycle.on_startup()


@app.on_event("shutdown")
def startup():
    logger.info("System shutdown initiated")
    LifeCycle.on_shutdown()


@app.get("/", include_in_schema=False)
async def redirect():
    return RedirectResponse("/docs")
