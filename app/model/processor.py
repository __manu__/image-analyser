from enum import Enum
from typing import List, Optional, Dict

from pydantic import BaseModel, Field


class ProcessorStatus(Enum):
    SUCCESS = "SUCCESS"
    FAILURE = "FAILURE"
    DEFERRED = "DEFERRED"


class StatusResponse(BaseModel):
    errors: Optional[List[str]] = Field(description="Error messages")
    warn: Optional[List[str]] = Field(description="Warning messages")
    info: Optional[List[str]] = Field(description="Additional info")
    status: ProcessorStatus = Field(description="Status")


class ProcessingStatus(StatusResponse):
    image_id: str = Field(description="Image Id")
    instructions: Optional[List[str]] = Field(description="Instructions messages")
    blurry: Optional[bool] = Field(description="Marks whether image is blurry or not")
    result_id: Optional[str] = Field(description="Resultant image id")
    model_label: Optional[str] = Field(description="Model label")

    def can_proceed(self):
        return self.status == ProcessorStatus.SUCCESS


class ImageProcessingStatus(ProcessingStatus):
    image_uri: Optional[str] = Field(description="Image URI")
    result_image_uri: Optional[str] = Field(description="Result Image URI")

    def can_proceed(self):
        return super().can_proceed() and self.result_image_uri


class MRCNNDetectionData(BaseModel):
    class_name: str = Field(description="Class")
    score: float = Field(description="Score")
    bbox: str = Field(description="BBox")


class MRCNNDetection(BaseModel):
    image_height: int = Field(description="Image height")
    image_width: int = Field(description="Image width")
    details: Optional[List[MRCNNDetectionData]] = Field(description="Details")


class MRCNNImageProcessingStatus(ImageProcessingStatus):
    detections: Optional[MRCNNDetection] = Field(description="Detection data")

    def can_proceed(self):
        return super().can_proceed() and self.detections


