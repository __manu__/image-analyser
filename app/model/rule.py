from typing import List, Optional, Dict

from pydantic import BaseModel


class Then(BaseModel):
    errors: Optional[List[str]]
    warn: Optional[List[str]]
    info: Optional[List[str]]
    instructions: Optional[List[str]]


class Rule(BaseModel):
    when: Dict
    then: Then


class RulesData(BaseModel):
    rules: Optional[List[Rule]]
