from typing import Dict

from app.accessories.accessory_units import MLAccessory
from app.config import SystemConfig
from app.interface.processor import ImageProcessor
from app.interface.rule_processor import RuleProcessor
from app.service.mock_processor import MockProcessor
from app.service.mrcnn_processor import MRCNNProcessor
from app.service.rule_processor import MRCNNRuleProcessor

MOCK_PROCESSOR = MockProcessor()

A = "a"
B = "b"
GENERIC = "generic"


class Processor(object):
    def __init__(self, image_processor: ImageProcessor, rule_processor: RuleProcessor) -> None:
        super().__init__()
        self.image_processor = image_processor
        self.rule_processor = rule_processor

    image_processor: ImageProcessor
    rule_processor: RuleProcessor


class ProcessorResolver:
    _implementation: Dict

    @staticmethod
    def load():
        ProcessorResolver._implementation = {
            "mock": Processor(image_processor=MOCK_PROCESSOR, rule_processor=MOCK_PROCESSOR),
            A: Processor(
                image_processor=MRCNNProcessor(MLAccessory.get_model_config(A), MLAccessory.get_local_model(A)),
                rule_processor=MRCNNRuleProcessor(MLAccessory.get_rule_config(A))
            ),
            B: Processor(
                image_processor=MRCNNProcessor(MLAccessory.get_model_config(B), MLAccessory.get_local_model(B)),
                rule_processor=MRCNNRuleProcessor(MLAccessory.get_rule_config(B))
            ),
            GENERIC: lambda label: Processor(
                image_processor=MRCNNProcessor(MLAccessory.get_model_config(label), MLAccessory.get_local_model(label)),
                rule_processor=MRCNNRuleProcessor(MLAccessory.get_rule_config(label))
            )
        }

    @staticmethod
    def get() -> Processor:
        return ProcessorResolver.get_by_label(SystemConfig.get("default_image_processor", "mock"))

    @staticmethod
    def get_by_label(model_label: str) -> Processor:
        return ProcessorResolver._implementation.get(model_label) or \
               ProcessorResolver._implementation.get(GENERIC)(model_label)
