from loguru import logger

from app.accessories.accessory_units import MLAccessory
from app.config import SystemConfig
from app.resolver.processor import ProcessorResolver


class LifeCycle(object):

    @staticmethod
    def on_startup():
        SystemConfig.load()
        MLAccessory.load()
        ProcessorResolver.load()
        logger.info("System actions on startup complete")

    @staticmethod
    def on_shutdown():
        logger.info("System actions on shutdown complete")

    @staticmethod
    def reload():
        LifeCycle.reload_ml()
        LifeCycle.reload_processors()

    @staticmethod
    def reload_ml():
        MLAccessory.load_forced(True)

    @staticmethod
    def reload_processors():
        ProcessorResolver.load()
