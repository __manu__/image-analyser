from app.service.model_store import ModelServer


class MLAccessory:
    _model_server: ModelServer = None

    @staticmethod
    def load():
        MLAccessory._model_server = ModelServer()
        MLAccessory._model_server.refresh()

    @staticmethod
    def load_forced(refresh_always: bool):
        MLAccessory._model_server = ModelServer()
        MLAccessory._model_server.refresh_forced(refresh_always=refresh_always)

    @staticmethod
    def get_model_config(model_label: str):
        return MLAccessory._model_server.get_config(model_label)

    @staticmethod
    def get_local_model(model_label: str):
        return MLAccessory._model_server.get_local_model(model_label)

    @staticmethod
    def get_rule_config(model_label: str):
        return MLAccessory._model_server.get_rule_config(model_label)
