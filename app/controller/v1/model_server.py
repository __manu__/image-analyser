from typing import Optional, Dict

from fastapi import APIRouter
from starlette.background import BackgroundTasks

from app.accessories.lifecycle_hooks import LifeCycle
from app.model.processor import ProcessorStatus

router = APIRouter(prefix="/api/v1/models")


@router.post(
    "/refresh", tags=["System"],
    summary="Refresh system",
    response_model=Optional[Dict],
    response_model_exclude_none=True
)
def refresh_models(background_tasks: BackgroundTasks) -> {}:
    background_tasks.add_task(LifeCycle.reload)
    return {"status": ProcessorStatus.SUCCESS, "message": "System refresh refresh started"}


@router.post(
    "/refresh-process", tags=["System"],
    summary="Refresh processors",
    response_model=Optional[Dict],
    response_model_exclude_none=True
)
def refresh_models(background_tasks: BackgroundTasks) -> {}:
    background_tasks.add_task(LifeCycle.reload_processors())
    return {"status": ProcessorStatus.SUCCESS, "message": "Processor refresh refresh started"}


@router.post(
    "/refresh-ml", tags=["System"],
    summary="Refresh ML systems",
    response_model=Optional[Dict],
    response_model_exclude_none=True
)
def refresh_models(background_tasks: BackgroundTasks) -> {}:
    background_tasks.add_task(LifeCycle.reload_ml())
    return {"status": ProcessorStatus.SUCCESS, "message": "ML module refresh started"}
