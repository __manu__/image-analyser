from fastapi import APIRouter, Depends
from starlette.background import BackgroundTasks

from app.model.processor import ProcessingStatus
from app.service.engine import DefaultEngine
from app.utils.path_utils import PathUtils

router = APIRouter(prefix="/api/v1/processor")
TEST_IMAGE_STORE = "ml-test"
PathUtils.create_paths(TEST_IMAGE_STORE)


@router.post(
    "/process", tags=["Image Analysis"],
    summary="Process Images",
    response_model=ProcessingStatus,
    response_model_exclude_none=True
)
def process_image(image_id: str, background_tasks: BackgroundTasks, engine: DefaultEngine = Depends(DefaultEngine)) -> ProcessingStatus:
    return engine.process(image_id, background_tasks)


@router.post(
    "/process/{model}", tags=["Image Analysis"],
    summary="Process Images by specific model",
    response_model=ProcessingStatus,
    response_model_exclude_none=True
)
def process_image(image_id: str, model: str, background_tasks: BackgroundTasks, engine: DefaultEngine = Depends(DefaultEngine)) -> ProcessingStatus:
    return engine.process_by_label(model, image_id, background_tasks)


