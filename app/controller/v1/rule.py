from fastapi import APIRouter

from app.model.processor import StatusResponse, ProcessorStatus
from app.model.rule import RulesData
from app.resolver.processor import ProcessorResolver

router = APIRouter(prefix="/api/v1/rule")


@router.post(
    "", tags=["Rule"],
    summary="Add rule",
    response_model=StatusResponse,
    response_model_exclude_none=True
)
def add_rule(data: RulesData, label: str) -> StatusResponse:
    processors = ProcessorResolver.get_by_label(label)
    if not processors:
        return StatusResponse(status=ProcessorStatus.FAILURE, errors=["Invalid label"])
    return processors.rule_processor.update(rules=data)


@router.get(
    "", tags=["Rule"],
    summary="Add rule",
    response_model=RulesData,
    response_model_exclude_none=True
)
def get_rule(label: str) -> RulesData:
    processors = ProcessorResolver.get_by_label(label)
    if not processors:
        return RulesData()
    return processors.rule_processor.get()
