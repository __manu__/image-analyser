from fastapi import UploadFile, Depends, File, APIRouter
from starlette.responses import FileResponse

from app.model.processor import ProcessingStatus, ProcessorStatus
from app.service.image_store import ImageStore
from app.utils.path_utils import PathUtils

router = APIRouter(prefix="/api/v1/test")
TEST_IMAGE_STORE = "ml-test"
PathUtils.create_paths(TEST_IMAGE_STORE)


@router.post(
    "/image", tags=["Test Images"],
    summary="Save Images",
    response_model_exclude_none=True
)
async def process_image(image: UploadFile = File(...),
                        image_store: ImageStore = Depends(ImageStore)) -> ProcessingStatus:
    image_store.upload_image_obj(image.file, f"{TEST_IMAGE_STORE}/{image.filename}")
    return ProcessingStatus(image_id="test", status=ProcessorStatus.SUCCESS)


@router.get(
    "/image", tags=["Test Images"],
    summary="Download Images",
    response_model_exclude_none=True
)
async def process_image(file_name: str,
                        image_store: ImageStore = Depends(ImageStore)) -> FileResponse:
    path = f"{TEST_IMAGE_STORE}/{file_name}"
    image_store.download_image(path, path)
    return FileResponse(path=path, filename=file_name, media_type='application/octet-stream')
