from app.exception import Error


class NoDetectionException(Error):
    pass
