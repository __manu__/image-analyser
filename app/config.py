import os

from dotenv import load_dotenv, find_dotenv
from loguru import logger

is_env_file = os.getenv("CONFIG_SOURCE", "env_file") == "env_file"


class SystemConfig:
    _loaded: bool = False

    @staticmethod
    def load():
        logger.info("Loading from env file: {}", is_env_file)
        if is_env_file:
            logger.info("Loading from env file: image-analyser.env")
            load_dotenv(find_dotenv(filename='image-analyser.env', raise_error_if_not_found=True))
        SystemConfig._loaded = True

    @staticmethod
    def get(key: str, default=None):
        if not SystemConfig._loaded and is_env_file:
            SystemConfig.load()
        return os.getenv(key, default)

    @staticmethod
    def get_vital(key: str):
        if not SystemConfig._loaded:
            SystemConfig.load()
        env_val = os.getenv(key)
        if env_val:
            return env_val
        raise Exception(f"Mandatory parameter: {key} not available in env | Env file read: {is_env_file} ")


