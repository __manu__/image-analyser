FROM mambaorg/micromamba:0.22.0
COPY --chown=$MAMBA_USER:$MAMBA_USER env.yml /tmp/env.yml
RUN micromamba install --yes --name base -f /tmp/env.yml --channel conda-forge \
      pyopenssl=20.0.1  \
      python=3.7 \
    micromamba clean --all --yes
RUN python -c 'import uuid; print(uuid.uuid4())' > /tmp/my_uuid
WORKDIR /app
COPY . .
EXPOSE 8000

#CMD ["uvicorn", "--reload", "--host=0.0.0.0", "--port=8000", "main:app"]
CMD ["python" , "run.py"]