# Image-Analyser

> **WARN:** The code is not reviewed or tested completely.

## Purpose
Image-analyser is supposed to analyse the dental images and quantitatively propose the quality of images.

## Prerequisites
- Poetry

## Configuration
- Go to image-analyser.env
    - image.processor may be changed to corresponding implementation.
    - The implementations are available in resolver/processor.py

## Run
```
Run scripts/launch.sh
     OR
Run run.py
```
    

## Build

### Micromamba
- Install micromamba as discussed in [docs](https://mamba.readthedocs.io/en/latest/installation.html)
- Go to `micromamba` installed path. Run `./micromamba shell init --shell=<shell> --prefix=<path of micromamba>`
- In project dir, run `micromamba create -n image-analyser-env-v2`  
- In project dir, run `micromamba install --yes -f env.yml`
- Set the IDE interpreter as `<path of micromamba>/envs/image-analyser-env-v2/bin/python`

### Docker
- Docker
    - Docker build not tested

Please read [micromamba docs](https://mamba.readthedocs.io/en/latest/user_guide/micromamba.html)
        
## API Docs
- Run the project
- Go to http://localhost:8000/docs or http://localhost:8000/redoc